//
//  AppDelegate.h
//  PartyMakerTeam
//
//  Created by Oleksandr Borovok on 11/5/16.
//  Copyright © 2016 Oleksandr Borovok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

